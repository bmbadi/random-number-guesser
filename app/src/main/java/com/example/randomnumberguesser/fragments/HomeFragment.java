package com.example.randomnumberguesser.fragments;

import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.example.randomnumberguesser.MainActivity;
import com.example.randomnumberguesser.R;
import com.example.randomnumberguesser.room.entities.Guess;
import com.example.randomnumberguesser.room.repositories.GuessRepository;
import com.example.randomnumberguesser.utils.Utils;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.Date;

public class HomeFragment extends Fragment {

    private MainActivity mainActivity;
    private int randomNumber, lowerLimit, upperLimit;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mainActivity = (MainActivity) getActivity();
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView txtInstruction = view.findViewById(R.id.txtInstruction);
        EditText edtNumber = view.findViewById(R.id.edtNumber);
        Button btnSubmit = view.findViewById(R.id.btnSubmit);

        mainActivity.setTitle("Home");
        randomNumber = initializePrompt(txtInstruction, edtNumber);

        btnSubmit.setOnClickListener(v -> {
            String gottenNumber = edtNumber.getText().toString().trim();
            if (gottenNumber.isEmpty()) {
                Toast.makeText(getContext(), "Kindly guess a number as per the above instructions", Toast.LENGTH_SHORT).show();
            } else {
                int guessedNumber = Integer.parseInt(gottenNumber);
                if (guessedNumber >= lowerLimit && guessedNumber <= upperLimit) {
                    final BottomSheetDialog dialog = new BottomSheetDialog(mainActivity);
                    View dialogView = View.inflate(mainActivity, R.layout.dialog_bottom, null);
                    dialog.setContentView(dialogView);
                    dialog.setCancelable(true);

                    final TextView txtTitle = dialogView.findViewById(R.id.txtTitle);
                    final TextView txtDesc = dialogView.findViewById(R.id.txtDesc);
                    final TextView txtRetry = dialogView.findViewById(R.id.txtRetry);
                    final TextView txtRecords = dialogView.findViewById(R.id.txtRecords);

                    String title, desc;

                    if (guessedNumber == randomNumber) {
                        title = "Correct";
                        desc = "Perfect, you guessed the correct number";
                    } else {
                        title = "Incorrect";
                        desc = "Sorry, the generated random number is <b>" + randomNumber + "</b>. Better luck next time";
                    }
                    txtTitle.setText(title);
                    txtDesc.setText(Html.fromHtml(desc));

                    Guess g = new Guess();
                    g.setGuessedNumber(guessedNumber);
                    g.setCorrectNumber(randomNumber);
                    g.setTimestamp(new Date().getTime());
                    GuessRepository repository = new GuessRepository(mainActivity.getApplication());
                    repository.recordGuess(g);

                    txtRetry.setOnClickListener(v1 -> dialog.dismiss());

                    txtRecords.setOnClickListener(v1 -> {
                        dialog.dismiss();
                        Navigation.findNavController(view).navigate(R.id.actionHomeToRecords);
                    });

                    dialog.setOnDismissListener(dialog1 -> randomNumber = initializePrompt(txtInstruction, edtNumber));
                    dialog.show();
                } else {
                    Toast.makeText(getContext(), "Kindly guess a number that is within the given range", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private int initializePrompt(TextView txtInstruction, EditText edtNumber) {
        //rangeDifference = max - min
        //small range to give the user a better chance of guessing the correct random number
        final int rangeDifference = 4;

        lowerLimit = Utils.getRandomNumber(1, 94);
        upperLimit = lowerLimit + rangeDifference;
        String prompt = "Guess the generated random number. It is in the range <b>" + lowerLimit + "</b> - <b>" + upperLimit + "</b>";
        if (Build.VERSION.SDK_INT >= 24) {
            txtInstruction.setText(Html.fromHtml(prompt, Html.FROM_HTML_MODE_COMPACT));
        } else {
            txtInstruction.setText(Html.fromHtml(prompt));
        }
        edtNumber.getText().clear();
        return Utils.getRandomNumber(lowerLimit, upperLimit);
    }
}
