package com.example.randomnumberguesser.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.MenuHost;
import androidx.core.view.MenuProvider;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.ViewModelProvider;

import com.example.randomnumberguesser.MainActivity;
import com.example.randomnumberguesser.R;
import com.example.randomnumberguesser.adapters.RecordAdapter;
import com.example.randomnumberguesser.room.entities.Guess;
import com.example.randomnumberguesser.room.viewmodels.GuessViewModel;
import com.inqbarna.tablefixheaders.TableFixHeaders;

import java.util.ArrayList;
import java.util.List;

public class RecordsFragment extends Fragment {
    private MainActivity mainActivity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mainActivity = (MainActivity) getActivity();
        return inflater.inflate(R.layout.fragment_records, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TableFixHeaders tableFixHeaders = view.findViewById(R.id.tableFixHeaders);
        TextView txtInfo = view.findViewById(R.id.txtInfo);
        RecordAdapter adapter = new RecordAdapter(getContext(), false);
        tableFixHeaders.setAdapter(adapter);
        updateTitle(adapter.isDisplayingAllRecords());

        final String NO_RECORDS = "No records found. Kindly attempt more guesses.";

        GuessViewModel guessViewModel = new ViewModelProvider(RecordsFragment.this).get(GuessViewModel.class);
        guessViewModel.getAllGuessesLiveData().observe(getViewLifecycleOwner(), guesses -> {
            //only called once

            List<Guess> correctGuesses = new ArrayList<>();
            for (Guess g : guesses) {
                if (g.getCorrectNumber() == g.getGuessedNumber()) {
                    correctGuesses.add(g);
                }
            }

            txtInfo.setText(correctGuesses.isEmpty() ? NO_RECORDS : "");

            adapter.getAllGuesses().addAll(guesses);
            adapter.getCorrectGuesses().addAll(correctGuesses);
            adapter.notifyDataSetChanged();
        });

        MenuHost menuHost = requireActivity();
        menuHost.addMenuProvider(new MenuProvider() {
            @Override
            public void onCreateMenu(@NonNull Menu menu, @NonNull MenuInflater menuInflater) {
                menuInflater.inflate(R.menu.records_menu, menu);
            }

            @Override
            public boolean onMenuItemSelected(@NonNull MenuItem menuItem) {
                int itemId = menuItem.getItemId();
                if (itemId == R.id.menuCorrectGuesses) {
                    if (adapter.isDisplayingAllRecords()) {
                        adapter.setDisplayingAllRecords(false);
                        updateTitle(adapter.isDisplayingAllRecords());
                        txtInfo.setText(adapter.getCorrectGuesses().isEmpty() ? NO_RECORDS : "");
                        adapter.notifyDataSetChanged();
                        return true;
                    }

                } else if (itemId == R.id.menuAllGuesses) {
                    if (!adapter.isDisplayingAllRecords()) {
                        adapter.setDisplayingAllRecords(true);
                        updateTitle(adapter.isDisplayingAllRecords());
                        txtInfo.setText(adapter.getAllGuesses().isEmpty() ? NO_RECORDS : "");
                        adapter.notifyDataSetChanged();
                        return true;
                    }

                }
                return false;
            }
        }, getViewLifecycleOwner(), Lifecycle.State.RESUMED);
    }

    private void updateTitle(boolean displayAllRecords) {
        String title = (displayAllRecords ? "All" : "Correct") + " Guesses";
        mainActivity.setTitle(title);
    }
}
