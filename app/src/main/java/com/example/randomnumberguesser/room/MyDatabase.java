package com.example.randomnumberguesser.room;

import android.app.Application;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.randomnumberguesser.room.dao.GuessDao;
import com.example.randomnumberguesser.room.entities.Guess;

@Database(version = 1, entities = {Guess.class})
public abstract class MyDatabase extends RoomDatabase {
    private static MyDatabase instance;

    public static synchronized MyDatabase getInstance(Application application) {
        if (instance == null) {
            instance = Room.databaseBuilder(application, MyDatabase.class, "NumberGuesser")
                    .build();
        }
        return instance;
    }

    public abstract GuessDao guessDao();
}
