package com.example.randomnumberguesser.room.entities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Guess {
    @PrimaryKey(autoGenerate = true)
    private Integer guessId;

    private long timestamp;
    private int correctNumber;
    private int guessedNumber;

    public Integer getGuessId() {
        return guessId;
    }

    public void setGuessId(Integer guessId) {
        this.guessId = guessId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getCorrectNumber() {
        return correctNumber;
    }

    public void setCorrectNumber(int correctNumber) {
        this.correctNumber = correctNumber;
    }

    public int getGuessedNumber() {
        return guessedNumber;
    }

    public void setGuessedNumber(int guessedNumber) {
        this.guessedNumber = guessedNumber;
    }
}
