package com.example.randomnumberguesser.room.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.randomnumberguesser.room.entities.Guess;

import java.util.List;

@Dao
public interface GuessDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void add(Guess guess);

    @Query("SELECT g.* FROM Guess g ORDER BY g.timestamp DESC")
    LiveData<List<Guess>> findAllLiveData();
}
