package com.example.randomnumberguesser.room.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.randomnumberguesser.room.entities.Guess;
import com.example.randomnumberguesser.room.repositories.GuessRepository;

import java.util.List;

public class GuessViewModel extends AndroidViewModel {

    private final LiveData<List<Guess>> allGuessesLiveData;

    public GuessViewModel(@NonNull Application application) {
        super(application);
        GuessRepository guessRepository = new GuessRepository(application);
        allGuessesLiveData = guessRepository.findAllGuessesLiveData();
    }

    public LiveData<List<Guess>> getAllGuessesLiveData() {
        return allGuessesLiveData;
    }
}
