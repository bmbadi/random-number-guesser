package com.example.randomnumberguesser.room.repositories;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.randomnumberguesser.room.MyDatabase;
import com.example.randomnumberguesser.room.dao.GuessDao;
import com.example.randomnumberguesser.room.entities.Guess;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class GuessRepository {
    private final GuessDao guessDao;

    public GuessRepository(Application application) {
        guessDao = MyDatabase.getInstance(application).guessDao();
    }

    public LiveData<List<Guess>> findAllGuessesLiveData() {
        return guessDao.findAllLiveData();
    }

    public void recordGuess(Guess guess) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> guessDao.add(guess));
    }
}
