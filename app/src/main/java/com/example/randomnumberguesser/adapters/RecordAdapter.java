package com.example.randomnumberguesser.adapters;

import android.content.Context;
import android.content.res.Resources;

import com.example.randomnumberguesser.R;
import com.example.randomnumberguesser.room.entities.Guess;
import com.example.randomnumberguesser.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class RecordAdapter extends TableAdapter {
    private final int widthSmall, widthMedium, widthLarge;
    private final int height;
    private final List<Guess> allGuesses, correctGuesses;
    private boolean displayAllRecords;
    private int rowCount;

    public RecordAdapter(Context context, boolean displayAllRecords) {
        super(context);
        this.allGuesses = new ArrayList<>();
        this.correctGuesses = new ArrayList<>();
        this.displayAllRecords = displayAllRecords;
        this.rowCount = 0;

        Resources resources = context.getResources();

        widthSmall = resources.getDimensionPixelSize(R.dimen.table_width_small);
        widthMedium = resources.getDimensionPixelSize(R.dimen.table_width_medium);
        widthLarge = resources.getDimensionPixelSize(R.dimen.table_width_large);
        height = resources.getDimensionPixelSize(R.dimen.table_height);
    }

    @Override
    public int getRowCount() {
        rowCount = displayAllRecords ? allGuesses.size() : correctGuesses.size();
        return rowCount;
    }

    @Override
    public int getColumnCount() {
        return displayAllRecords ? Column.values().length - 1 : Column.values().length - 2;
    }

    @Override
    public int getHeight(int row) {
        return height;
    }

    @Override
    public String getCellString(int row, int column) {
        Guess g = row == -1 ? null : (displayAllRecords ? allGuesses.get(row) : correctGuesses.get(row));
        if (column == Column.POS.index) {
            return row == -1 ? "#" : String.valueOf(rowCount - row);
        } else if (column == Column.DATE.index) {
            return row == -1 ? "Date" : Utils.timestampToString(g.getTimestamp());
        } else if (column == Column.NUMBER_GUESSED.index) {
            return row == -1 ? "Guessed" : String.valueOf(g.getGuessedNumber());
        } else if (column == Column.NUMBER_CORRECT.index) {
            return row == -1 ? "Correct" : String.valueOf(g.getCorrectNumber());
        } else {
            return "-";
        }
    }

    @Override
    public int getLayoutResource(int row, int column) {
        final int layoutResource;
        int itemViewType = getItemViewType(row, column);
        if (itemViewType == RowType.HEAD.viewType) {
            layoutResource = R.layout.table_header;
        } else if (itemViewType == RowType.EVEN.viewType) {
            layoutResource = R.layout.table_row_even;
        } else if (itemViewType == RowType.ODD.viewType) {
            layoutResource = R.layout.table_row_odd;
        } else {
            throw new RuntimeException("invalid layout type");
        }
        return layoutResource;
    }

    @Override
    public int getItemViewType(int row, int column) {
        if (row < 0) {
            return RowType.HEAD.viewType;
        } else {
            return row % 2 == 0 ? RowType.EVEN.viewType : RowType.ODD.viewType;
        }
    }

    @Override
    public int getViewTypeCount() {
        return RowType.values().length;
    }

    @Override
    public int getWidth(int column) {
        if (column == Column.POS.index) {
            return widthSmall;
        } else if (column == Column.DATE.index) {
            return widthLarge;
        }
        return widthMedium;
    }

    public List<Guess> getCorrectGuesses() {
        return correctGuesses;
    }

    public List<Guess> getAllGuesses() {
        return allGuesses;
    }

    public boolean isDisplayingAllRecords() {
        return displayAllRecords;
    }

    public void setDisplayingAllRecords(boolean displayAllRecords) {
        this.displayAllRecords = displayAllRecords;
    }

    private enum Column {
        POS(-1),
        DATE(0),
        NUMBER_GUESSED(1),
        NUMBER_CORRECT(2);

        private final int index;

        Column(int index) {
            this.index = index;
        }
    }

    private enum RowType {
        HEAD(0),
        EVEN(1),
        ODD(2);

        private final int viewType;

        RowType(int viewType) {
            this.viewType = viewType;
        }
    }
}
