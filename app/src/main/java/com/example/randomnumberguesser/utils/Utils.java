package com.example.randomnumberguesser.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Utils {
    public static int getRandomNumber(int lowerLimit, int upperLimit) {
        upperLimit++;
        double u = Math.random();
        // a + u(b-a)
        return lowerLimit + (int) (u * (double) (upperLimit - lowerLimit));
    }

    public static String timestampToString(long timestamp) {
        SimpleDateFormat sdf = new SimpleDateFormat("d-MMM-yyyy HH:mm:ss", Locale.UK);
        Date date = new Date(timestamp);
        return sdf.format(date);
    }
}
