package com.example.randomnumberguesser;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.pressKey;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.doesNotExist;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.DrawerActions.open;
import static androidx.test.espresso.matcher.RootMatchers.withDecorView;
import static androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;

import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;

import com.example.randomnumberguesser.utils.Utils;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class NumberGuessTest {
    @Rule
    public ActivityScenarioRule<MainActivity> mainActivityRule = new ActivityScenarioRule<>(MainActivity.class);

    private View decorView;

    @Before
    public void setUp() {
        mainActivityRule.getScenario().onActivity(activity -> decorView = activity.getWindow().getDecorView());
    }

    @Test
    public void noNumberTest() {
        onView(withId(R.id.txtInstruction)).check(matches(isDisplayed()));
        onView(withId(R.id.edtNumber)).check(matches(isDisplayed()));
        onView(withId(R.id.btnSubmit)).check(matches(isDisplayed()))
                .perform(click());

        onView(withText("Kindly guess a number as per the above instructions"))
                .inRoot(withDecorView(not(decorView)))
                .check(matches(isDisplayed()));
    }

    @Test
    public void invalidNumberTest() {
        onView(withId(R.id.txtInstruction)).check(matches(isDisplayed()));
        onView(withId(R.id.edtNumber)).check(matches(isDisplayed()))
                .perform(typeText("652"), pressKey(KeyEvent.KEYCODE_ENTER));
        onView(withId(R.id.btnSubmit)).check(matches(isDisplayed()))
                .perform(click());

        onView(withText("Kindly guess a number that is within the given range"))
                .inRoot(withDecorView(not(decorView)))
                .check(matches(isDisplayed()));
    }

    @Test
    public void validNumberAndRetryTest() {
        testValidNumber();
        onView(withId(R.id.txtRetry)).check(matches(isDisplayed()))
                .perform(click());
        onView(withId(R.id.layoutDialogBottom)).check(doesNotExist());
    }

    @Test
    public void validNumberAndViewRecordTest() {
        testValidNumber();
        onView(withId(R.id.txtRecords)).check(matches(isDisplayed()))
                .perform(click());
        onView(withId(R.id.tableFixHeaders)).check(matches(isDisplayed()));
    }

    private void testValidNumber() {
        onView(withId(R.id.txtInstruction)).check(matches(isDisplayed()));
        String gottenInstruction = getText(withId(R.id.txtInstruction));
        int len = gottenInstruction.length();
        int higherLimit = Integer.parseInt(gottenInstruction.substring(len - 2));
        int choice = Utils.getRandomNumber(higherLimit - 4, higherLimit);

        onView(withId(R.id.edtNumber)).check(matches(isDisplayed()))
                .perform(typeText(String.valueOf(choice)), pressKey(KeyEvent.KEYCODE_ENTER));
        onView(withId(R.id.btnSubmit)).check(matches(isDisplayed()))
                .perform(click());
        onView(withId(R.id.layoutDialogBottom)).check(matches(isDisplayed()));

    }

    @Test
    public void testDisplayRecordsFragment() {
        onView(withId(R.id.drawer_layout))
                .check(matches(isDisplayed()))
                .perform(open());
        onView(withId(R.id.navRecords))
                .check(matches(isDisplayed()))
                .perform(click());
        onView(withId(R.id.tableFixHeaders)).check(matches(isDisplayed()));
    }


    String getText(final Matcher<View> matcher) {
        final String[] stringHolder = {null};
        onView(matcher).perform(new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return isAssignableFrom(TextView.class);
            }

            @Override
            public String getDescription() {
                return "getting text from a TextView";
            }

            @Override
            public void perform(UiController uiController, View view) {
                TextView tv = (TextView) view; //Save, because of check in getConstraints()
                stringHolder[0] = tv.getText().toString();
            }
        });
        return stringHolder[0];
    }
}
