# Random Number Guesser
This Android Application tracks the number of times a user guesses a randomly generated integer correctly.

To give the user a better chance of guessing the correct random number, the user can only guess from a range of 5 numbers. To make the app more interesting, the range will be chosen randomly from the range 1 - 98.

## Installation
Clone this repository and import into **Android Studio**
```bash
git clone https://gitlab.com/bmbadi/random-number-guesser.git
```

## Screenshots
<p float="left">
  <img src="/readme/Screenshot_20221101_153437.png" width="200" /> 
  <img src="/readme/Screenshot_20221101_153426.png" width="200" />  
  <img src="/readme/Screenshot_20221101_153356.png" width="200" /> 
</p>
